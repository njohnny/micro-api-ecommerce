package demo.model;


public class LogisticsOrderDTO  {   

	private Long orderId;
	
	
	private Boolean aproved = false;

	public LogisticsOrderDTO() {
		
	}
	public Long getOrderId() {
		return orderId;
	}

	public void setOrderId(Long orderId) {
		this.orderId = orderId;
	}



	public Boolean getAproved() {
		return aproved;
	}

	public void setAproved(Boolean aproved) {
		this.aproved = aproved;
	}



}