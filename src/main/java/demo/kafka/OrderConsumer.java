package demo.kafka;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import demo.ApplicationEcommerce;
import demo.model.LogisticsOrderDTO;
import demo.model.PaymentOrderDTO;
import demo.service.OrderService;

@Component
public class OrderConsumer {
	
	@Autowired
	private OrderService orderService;
	
	private static final Logger LOG = LoggerFactory.getLogger(ApplicationEcommerce.class);


	  @KafkaListener(topics = "${spring.kafka.consumer.topics}", autoStartup = "${spring.kafka.consumer.enabled}",
			  properties= {"spring.json.value.default.type=demo.model.PaymentOrderDTO"})
	  public void onMessage(ConsumerRecord<String, Object> record) {
		  
		 if(record.value() instanceof PaymentOrderDTO) {
			 PaymentOrderDTO orderPaymentApproved = (PaymentOrderDTO) record.value();
			  LOG.info("E-COMMERCE: CONSUMER-[payment-APPROVED] - Received record [{}], topic: {}, partition: {}, offset: {}",  record.value(), record.topic(), record.partition(), record.offset());
			  
			  orderService.updateOrderPayment(orderPaymentApproved);
			  LOG.info("E-COMMERCE: Invocando serviço para agendar data de entrega do pedido !");
		 }else if(record.value() instanceof LogisticsOrderDTO) {
			 this.onMessageDelivery(record);
		 }
	  }
	  
	  @KafkaListener(topics = "${app.kafka.consumer.topics}", autoStartup = "${app.kafka.consumer.enabled}",
			  properties= {"spring.json.value.default.type=demo.model.LogisticsOrderDTO"})
	  public void onMessageDelivery(ConsumerRecord<String, Object> record) {
	    
		  LogisticsOrderDTO logisticsOrderScheduled = (LogisticsOrderDTO)record.value();
		  LOG.info("E-COMMERCE: CONSUMER-[delivery-SCHEDULED] - Received record [{}], topic: {}, partition: {}, offset: {}",  record.value(), record.topic(), record.partition(), record.offset());
		  
		  orderService.updateOrderDelivery(logisticsOrderScheduled);
		  LOG.info("E-COMMERCE: Invocando serviço de notificações para informar data de entrega do pedido !");
	  }
	  
}
