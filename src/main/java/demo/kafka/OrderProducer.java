package demo.kafka;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import demo.ApplicationEcommerce;
import demo.model.PaymentOrderDTO;

@Component
public class OrderProducer {
	
	private static final Logger LOG = LoggerFactory.getLogger(ApplicationEcommerce.class);
	
	@Autowired
	private KafkaTemplate<String, Object> templateProducer;
	
	public void send(String topic, PaymentOrderDTO submissionOrderDto) {
		templateProducer.send(topic, submissionOrderDto);
		
		LOG.info("E-COMMERCE: Sent method Finalized [{}] to topic [{}]", submissionOrderDto, topic);
	}

}
