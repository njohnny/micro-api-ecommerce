package demo.service;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import demo.entities.CustomerEntity;
import demo.entities.OrderEntity;
import demo.kafka.OrderProducer;
import demo.model.CustomerDTO;
import demo.model.LogisticsOrderDTO;
import demo.model.PaymentOrderDTO;
import demo.repository.order.OrderRepository;


@Service
public class OrderService {


	@Autowired
	private OrderRepository repository;
	
	@Autowired
	private OrderProducer producer;
	
	@Autowired
	private CustomerService customerService;
	
	
	@Value("${app.services.payment.uri}")
	private String servicePaymentURI;


	public List<OrderEntity> getOrders() {

		return this.repository.findAll();			
	}


	public OrderEntity getOrder(Long orderId) {

		return this.repository.findById(orderId).get();			
	}


	public OrderEntity create(OrderEntity order) {

		return this.repository.save(order);			
	}


	public OrderEntity update(OrderEntity order) {

		return this.repository.save(order);			
	}


	public boolean delete(Long id) {

		this.repository.deleteById(id);

		return true;			
	}

	
	
	//@Transactional
	public OrderEntity processOrder(OrderEntity order) {
			
		
		/******  create orde *****/
		order.setStatus("SUBMITTED");
		order = this.repository.saveAndFlush(order);
		
		PaymentOrderDTO submissionOrder = new PaymentOrderDTO();
		submissionOrder.setOrderId(order.getId());
		submissionOrder.setValue(order.getTotalValue());
		submissionOrder.setAproved(false);
		CustomerDTO customerFound = customerService.associateOrderToClient(order);
		submissionOrder.setCustomer(customerFound);
		
	
		producer.send("orders-SUBMISSION", submissionOrder);
		
		
		//OrderEntity order = this.repository.findById(orderPost.getId()).get();
				
		
		
		
		/***** call FINANCE service (SYNCHRONOUSLY) *****/
//		
//		PaymentOrderDTO requestFinance = new PaymentOrderDTO();
//		requestFinance.setOrderId(order.getId());
//		requestFinance.setFiscalNumber(order.getCustomer().getFiscalNumber());
//		requestFinance.setValue(order.getTotalValue());
//		
//		
//						
//		RestTemplate restTemplate = new RestTemplate();	
//		ResponseEntity<PaymentOrderDTO> responseFinance = restTemplate.postForEntity(servicePaymentURI, requestFinance, PaymentOrderDTO.class);		
//		
//		
//		if (responseFinance.getBody().getAproved()) {
//			
//			// update order status
//			order.setStatus("PAYMENT_APPROVED");
//			this.update(order);
//			
//			// SEND EMAIL AND SMS TO CUSTOMERS			
//			System.out.println("Email Payment: Dear " + order.getCustomer().getName() + " Paymment for your order " + order.getCode() + " was processed.");	
//		}
//		
//
//		
//		/****** call LOGISTIC SERVICE service (SYNCHRONOUSLY)  *****/
//		/* TO DO 
//		 * 
//		 * */		
//		boolean callLogisticOk = true;
//		
//		
//		if (callLogisticOk) {
//			
//			// update order status
//			order.setStatus("DELIVERY_SCHEDULED");
//			this.update(order);
//			
//			// SEND EMAIL AND SMS TO CUSTOMERS	
//			System.out.println("Email Delivery: Dear " + order.getCustomer().getName() + " Your order " + order.getCode() + " was will be delivery before 2022/XX/XX ");
//			
//		}
//		
		
		
		return order;		
	}


	


	public OrderEntity updateOrderPayment(PaymentOrderDTO orderPaymentApproved) {
		
		Long orderId = orderPaymentApproved.getOrderId();
		
		OrderEntity orderFound = this.getOrder(orderId);
		
		if(orderFound != null && orderFound.getStatus().equals("SUBMITTED")) {
			orderFound.setStatus("APPROVED");
			this.update(orderFound);
		System.out.println("ORDER UPDATED !! - ORDER APROVED");
		}
		return orderFound;
		
	}


	public OrderEntity updateOrderDelivery(LogisticsOrderDTO logisticsOrderScheduled) {
		Long orderId = logisticsOrderScheduled.getOrderId();
		
		OrderEntity orderFound = this.getOrder(orderId);
		
		if(orderFound != null && orderFound.getStatus().equals("APPROVED")) {
			orderFound.setStatus("DELIVERY_SCHEDULED");
			this.update(orderFound);
			System.out.println("ORDER UPDATED !! - DELIVERY SCHEDULED");
		}
		
		return orderFound;
		
	}

		
	



}
