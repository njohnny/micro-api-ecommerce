package demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import demo.entities.CustomerEntity;
import demo.entities.OrderEntity;
import demo.model.CustomerDTO;
import demo.model.PaymentOrderDTO;
import demo.repository.customer.CustomerRepository;

@Service
public class CustomerService {
	
	@Autowired
	private CustomerRepository repository;
	
	public CustomerEntity getCustomer(Long customerId) {
		
		return this.repository.findById(customerId).get();
	}
	
	public List<CustomerEntity> getCustomers() {
		
		return this.repository.findAll();
	}
	
	public CustomerEntity createCustomer(CustomerEntity customer) {
		
		return this.repository.save(customer);
	}
	
	public CustomerEntity updateCustomer(CustomerEntity customer) {
		return this.repository.save(customer);
	}
	
	public boolean delete(Long customerId) {
		
		this.repository.deleteById(customerId);
		
		return true;
	}
	
	
	public CustomerDTO associateOrderToClient(OrderEntity order) {
		CustomerEntity customer = new CustomerEntity();
		CustomerEntity customerFound = new CustomerEntity();
		CustomerDTO customerToReturn = new CustomerDTO();
		
	try {
		customer = order.getCustomer();
		customerFound = this.repository.findById(customer.getId()).get();
		if(customer != null) {
			customerToReturn.setEmail(customerFound.getEmail());
			customerToReturn.setName(customerFound.getName());
			customerToReturn.setFiscalNumber(customerFound.getFiscalNumber());
		}
	return customerToReturn;
	
	} catch (Exception e) {
		System.out.println("Entidade - CUSTOMER não existe, criando nova entidade"  );
		//customer.setId(order.getId());
		customer.setEmail("teste@email.com");
		customer.setName("João");
		customer.setFiscalNumber("000000000F");
		this.createCustomer(customer);
		
		customerToReturn.setName(customer.getName());
		customerToReturn.setEmail(customer.getEmail());
		customerToReturn.setFiscalNumber(customer.getFiscalNumber());
		
		return customerToReturn;
	}

	}

}
