package demo.webapi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import demo.entities.OrderEntity;
import demo.kafka.OrderProducer;
import demo.model.PaymentOrderDTO;
import demo.service.OrderService;



@RestController 
public class Controller {


	
	@Autowired
	private OrderService orderService;
	
	@RequestMapping(value="/", method = RequestMethod.GET)	
	public ResponseEntity<String> root() {
			
		return new ResponseEntity<String>("<hr>Kafka Cliente App is ON</hr> <br><br><b>Note: use /producer?topic=order_requested&message= to publish a menssage into Kafka Queue ! </b>",HttpStatus.OK);
		
	}
	
	
	@RequestMapping(value="/producer", method = RequestMethod.POST)	
	public ResponseEntity<String> producer(@RequestBody OrderEntity paymentOrder) {
		
		try {
			orderService.processOrder(paymentOrder);
		} catch (Exception e) {
			return new ResponseEntity<String>(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase(),HttpStatus.INTERNAL_SERVER_ERROR);
		} 
					
		
		// SUCCESS
		return new ResponseEntity<String>(HttpStatus.OK.getReasonPhrase(),HttpStatus.OK);
	}
	
	

}



